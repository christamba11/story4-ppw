django>=2.2.*
datetime
gunicorn>=19.7.1
psycopg2-binary
whitenoise>=4.1.*
