from django.shortcuts import render,redirect
from .models import Jadwal
from . import forms
# Create your views here.

'''Create new item of jadwal'''
def create_item(request):
    form = forms.FormJadwal()
    if request.method == "POST":
        form = forms.FormJadwal(request.POST)
        if form.is_valid():
            form.save()
            return redirect('jadwal:list_jadwal')
    return render(request,'jadwal/form_jadwal.html', {'form':form} )

'''Views for outputing all items of jadwal'''
def list_jadwal(request):
    daftar_item = Jadwal.objects.order_by('tanggal', 'waktu')
    response = {
        'daftar_item' :daftar_item,
    }
    return render(request, 'jadwal/list_jadwal.html', response)

'''Views for deleting item'''
def delete_item(request):
    if request.method == "POST":
        id = request.POST['id']
        Jadwal.objects.get(id = id).delete()
    return redirect('jadwal:list_jadwal')



