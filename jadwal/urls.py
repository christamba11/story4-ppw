from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from . import views

app_name = "jadwal"
urlpatterns = [
    path('', views.list_jadwal, name="list_jadwal"),
    path('create_jadwal/', views.create_item, name ="create_jadwal"),
    path('delete_jadwal/', views.delete_item)
]