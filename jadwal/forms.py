from . import models
from django import forms


class FormJadwal(forms.ModelForm):
    nama_kegiatan = forms.CharField(
        widget= forms.TextInput(
            attrs= {
                "class" : "txt-form-large",
                "required"  : True,
                "placeholder"   : "Name of Activity",
            }
        )
    )
    hari = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form",
                "required"  : True,
                "placeholder" : "Day",
            }
        )
    )
    tanggal = forms.DateField(
        widget= forms.SelectDateWidget(
            attrs = {
                "class" : "pilih-tanggal",
                "required" : True,
            }
        )

    )
    waktu = forms.TimeField(
        widget = forms.TimeInput(
            attrs = {
                "class" : "txt-form",
                "placeholder"  : "00:00",
                "required"  : True,
            }
        )
    )
    tempat = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form",
                "placeholder" : "Location",
                "required"  : True,
            }
        )
    )
    kategori = forms.CharField(
        widget = forms.TextInput(
            attrs = {
                "class" : "txt-form",
                "placeholder" : "Event's Category",
                "required"  : True,
            }
        )
    )
    class Meta:
        model = models.Jadwal
        fields = [
            'nama_kegiatan', 'hari', 'tanggal', 'waktu', 'tempat', 'kategori',
        ]