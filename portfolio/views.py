from django.shortcuts import render

def homeView(request):
    return render(request,'portfolio/homepage.html')

def projectView(request):
    return render(request, 'portfolio/my-projects.html')

def aboutView(request):
    return render(request, 'portfolio/about.html')

def skillsView(request):
    return render(request, 'portfolio/skills.html')

def experiencesView(request):
    return render(request, 'portfolio/experiences.html')