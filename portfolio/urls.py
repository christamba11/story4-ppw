from django.conf.urls import url
from django.contrib import admin
from django.urls import include, path
from . import views

app_name="portfolio"

urlpatterns = [
    path('', views.homeView, name="homepage"),
    path('projects/', views.projectView, name="projects"),
    path('about/', views.aboutView, name="about"),
    path('skills/', views.skillsView, name="skills"),
    path("experiences/", views.experiencesView, name="experiences")
]
